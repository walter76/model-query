﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommandLine;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.FindSymbols;
using Microsoft.CodeAnalysis.MSBuild;

using ModelQuery.Cli.Importer;
using ModelQuery.Cli.Model;

namespace ModelQuery.Cli
{
    internal class Program
    {
        private static Dictionary<string, List<string>> RunGetUsagesOfMethods(Project project)
        {
            var tasks = new List<Task<Dictionary<string, List<string>>>>();

            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                tasks.Add(GetUsagesOfMethodsAsync(document));
            }

            Task.WaitAll(tasks.ToArray());

            var totalUsagesOfMethods = new Dictionary<string, List<string>>();

            foreach (var task in tasks)
            {
                if (!task.IsCompletedSuccessfully)
                {
                    continue;
                }

                foreach (var methodUsages in task.Result)
                {
                    totalUsagesOfMethods.Add(methodUsages.Key, methodUsages.Value);
                }
            }

            return totalUsagesOfMethods;
        }

        private static async Task<Dictionary<string, List<string>>> GetUsagesOfMethodsAsync(Document document)
        {
            var usagesOfMethods = new Dictionary<string, List<string>>();
            var syntaxTree = await document.GetSyntaxTreeAsync();
            var collector = new MethodCollector();
            collector.Visit(syntaxTree.GetRoot());

            foreach (var methodDeclaration in collector.Methods)
            {
                var model = document.GetSemanticModelAsync().Result;
                var symbol = model.GetDeclaredSymbol(methodDeclaration);

                var referencesToM = SymbolFinder
                    .FindReferencesAsync(
                        symbol, document.Project.Solution).Result;

                var locations = new List<string>();
                foreach (var reference in referencesToM)
                {
                    foreach (var location in reference.Locations)
                    {
                        locations.Add(location.Location.GetMappedLineSpan().ToString());
                    }
                }

                usagesOfMethods.Add(methodDeclaration.Identifier.ToFullString(), locations);
            }

            return usagesOfMethods;
        }

        private static void Run(Options options)
        {
            MSBuildLocator.RegisterDefaults();
            var workspace = MSBuildWorkspace.Create();
            var project = workspace.OpenProjectAsync(options.ProjectPath).Result;
            var csharpModel = new CSharpModel();

            var allNamespaceEntities = new NamespaceImporter().FindAll(project);
            foreach (var namespaceEntity in allNamespaceEntities)
            {
                csharpModel.AddNamespace(namespaceEntity);
            }

            var allClassEntities = new ClassesImporter().FindAll(project);
            foreach (var classEntity in allClassEntities)
            {
                csharpModel.AddClass(classEntity);
            }

            if (options.Namespace)
            {
                foreach (var namespaceEntity in csharpModel.Namespaces)
                {
                    System.Console.WriteLine(namespaceEntity.Identifier);
                }
            }
            else if (options.Classes)
            {
                foreach (var classEntity in csharpModel.Classes)
                {
                    System.Console.WriteLine(classEntity.Identifier);
                }
            }
            else if (options.MethodUsages)
            {
                var usages = RunGetUsagesOfMethods(project);

                foreach (var usage in usages)
                {
                    System.Console.WriteLine(usage.Key);
                    foreach (var c in usage.Value)
                    {
                        System.Console.WriteLine($"  {c}");
                    }
                }
            }
        }

        // https://github.com/dotnet/roslyn-sdk/blob/master/samples/CSharp/FormatSolution/Program.cs
        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args).WithParsed<Options>(Run);
        }
    }
}
