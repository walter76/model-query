using CommandLine;

namespace ModelQuery.Cli
{
    internal class Options
    {
        [Option('n', "namespace", Required = false, HelpText = "List all namespaces in project")]
        public bool Namespace { get; set; }

        [Option('c', "classes", Required = false, HelpText = "List all classes in project")]
        public bool Classes { get; set; }

        [Option('m', "methods", Required = false, HelpText = "List all method usages in project")]
        public bool MethodUsages { get; set; }

        [Value(0, MetaName = "project file", HelpText = "Project file to query.", Required = true)]
        public string ProjectPath { get; set; }
    }
}
