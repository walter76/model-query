using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ModelQuery.Cli
{
    internal class MethodCollector : CSharpSyntaxWalker
    {
        public List<MethodDeclarationSyntax> Methods { get; }

        public MethodCollector()
        {
            Methods = new List<MethodDeclarationSyntax>();
        }

        public override void VisitMethodDeclaration(MethodDeclarationSyntax node)
        {
            Methods.Add(node);

            base.VisitMethodDeclaration(node);
        }
    }
}
