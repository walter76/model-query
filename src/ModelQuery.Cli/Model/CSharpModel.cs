﻿using System.Collections.Generic;

namespace ModelQuery.Cli.Model
{
    internal class CSharpModel
    {
        private readonly List<NamespaceEntity> _namespaces = new List<NamespaceEntity>();
        private readonly List<ClassEntity> _classes = new List<ClassEntity>();

        public IReadOnlyCollection<NamespaceEntity> Namespaces => _namespaces;
        public IReadOnlyCollection<ClassEntity> Classes => _classes;

        public void AddNamespace(NamespaceEntity namespaceEntity)
        {
            _namespaces.Add(namespaceEntity);
        }

        public void AddClass(ClassEntity classEntity)
        {
            _classes.Add(classEntity);
        }
    }
}