﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using ModelQuery.Cli.Model;

namespace ModelQuery.Cli.Importer
{
    internal class ClassesImporter
    {
        public IReadOnlyCollection<ClassEntity> FindAll(Project project)
        {
            var tasks = new List<Task<List<ClassDeclarationSyntax>>>();

            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                tasks.Add(GetClassesAsync(document));
            }

            Task.WaitAll(tasks.ToArray());

            var totalClassDeclarations = new List<ClassDeclarationSyntax>();

            foreach (var task in tasks)
            {
                if (!task.IsCompletedSuccessfully)
                {
                    continue;
                }

                totalClassDeclarations.AddRange(task.Result);
            }

            return totalClassDeclarations
                .Select(d => new ClassEntity { Identifier = d.Identifier.ValueText })
                .ToList()
                .AsReadOnly();
        }

        private static async Task<List<ClassDeclarationSyntax>> GetClassesAsync(Document document)
        {
            var syntaxTree = await document.GetSyntaxTreeAsync();
            var collector = new ClassCollector();
            collector.Visit(syntaxTree.GetRoot());

            return collector.Classes.ToList();
        }
    }
}
