using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ModelQuery.Cli.Importer
{
    internal class ClassCollector : CSharpSyntaxWalker
    {
        public List<ClassDeclarationSyntax> Classes { get; }

        public ClassCollector()
        {
            Classes = new List<ClassDeclarationSyntax>();
        }

        public override void VisitClassDeclaration(ClassDeclarationSyntax node)
        {
            Classes.Add(node);

            base.VisitClassDeclaration(node);
        }
    }
}
