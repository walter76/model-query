﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using ModelQuery.Cli.Model;

namespace ModelQuery.Cli.Importer
{
    internal class NamespaceImporter
    {
        public IReadOnlyCollection<NamespaceEntity> FindAll(Project project)
        {
            var allNamespaceDeclarations = new List<NamespaceDeclarationSyntax>();

            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                var syntaxTreeRoot = document.GetSyntaxTreeAsync().Result.GetRoot();
                var namespaceDeclarations =
                  syntaxTreeRoot
                  .DescendantNodes()
                  .Where(n => n is NamespaceDeclarationSyntax)
                  .Cast<NamespaceDeclarationSyntax>();

                allNamespaceDeclarations.AddRange(namespaceDeclarations);
            }

            return allNamespaceDeclarations
                .Select(d => d.Name.ToString())
                .Distinct()
                .Select(identifier => new NamespaceEntity { Identifier = identifier })
                .ToList()
                .AsReadOnly();
        }
    }
}
